#!/usr/bin/env bash

#Takes in bedpe files and pairs them to genome features using pairtobed. 
#Also pairs the bedpe file data to a backround distribution of the genome features that
#have been shuffled 50 times using shufflebed (i.e if you have 3 features the backround file
#has the same features mapped to random genome locations 50 times giving you a file with 150 entries).
#Currently only takes in 3 stages because it makes a boxplot tailered to showing 3 stages.

#Requires a directory structure be set up where there are directories with the stage names supplied 
#The output for a particular stage goes to the directory named that stage.
#The shuffled data is in the same directory as the original data.

function change_path(){
#Takes a file path and puts the basename onto newpath
#Example change_path(old_path/file.txt new_path) returns new_path/file.txt

_path=$1
_new_path=$2

echo "$_new_path"/`basename $_path`


}



function cat_to_path(){
#Takes a file path and adds string_to_cat to the
#file between the extension and the rest of the path
#Example cat_to_path(example.txt,1) returns example1.txt

_path=$1
_string_to_cat=$2


#Probably not the must efficient way of doing this but it works for now
file_name=`echo $_path | awk -F"\." '{print $1}'`
file_extension=`echo $_path | awk -F"\." '{print $2}'`
sep="."

echo $file_name$_string_to_cat$sep$file_extension


}

function change_and_cat_to_path(){
#Combines the actions of change_path and cat to path
#Example change_and_cat_to_path(old_path/file.txt new_path 1) returns new_path/file1.txt

_path=$1
_new_path=$2
_string_to_cat=$3

new_path_added=$(change_path "$_path" "$_new_path")

echo `cat_to_path "$new_path_added" "$_string_to_cat"`

}

function analyze_bedpe_by_stage(){

_stage1_name=$1
_stage1_bedpe=$2

_stage2_name=$3
_stage2_bedpe=$4

_stage3_name=$5
_stage3_bedpe=$6

_feature_bed_file=$7

_genomic_length_file=$8

_experiment_name=$9 #will be put under the boxplot for the experimental data

_type=${10} #Currently not wokring with both and type
_f=${11}

_boxplot_title=${12}

_boxplot_outfile=${13}

#Uncomment the block below to debug command line argument problems.

#counter=1
#while [[ $1 ]]
#	do
#		echo $counter $1
#		counter=`expr $counter + 1`
#		shift
#	done

#Hard coded Variables
pairtobed_extension=_pairtobed

backround_extension=_pairtobed_backround

feature_backround_extension=_shuffled

number_of_shuffles=50

backround=bk #Don't want it to long because graphs are being made automatically and may push off the important descriptor

#Create outfile pointing to directories in the current working directory with the stage names (These must already exist)

#Create filenames for the output of pairtobed

stage1_ptb=$(change_and_cat_to_path $_stage1_bedpe $_stage1_name $pairtobed_extension)
stage2_ptb=$(change_and_cat_to_path $_stage2_bedpe $_stage2_name $pairtobed_extension)
stage3_ptb=$(change_and_cat_to_path $_stage3_bedpe $_stage3_name $pairtobed_extension)

#Create filenames of the backound data

stage1_bk=$(change_and_cat_to_path $_stage1_bedpe $_stage1_name $backround_extension)
stage2_bk=$(change_and_cat_to_path $_stage2_bedpe $_stage2_name $backround_extension)
stage3_bk=$(change_and_cat_to_path $_stage3_bedpe $_stage3_name $backround_extension)


#Create file name for the shuffled feature data. Not changing the directory because the shuffled data can be used for each stage and so should stay in the original directory.
shuffled_data_file=$(cat_to_path $_feature_bed_file $feature_backround_extension)

#Put files into arrays so can loop through them

declare -a bedpe_input_files=($_stage1_bedpe $_stage2_bedpe $_stage3_bedpe)
declare -a pairtobed_outfiles=($stage1_ptb $stage2_ptb $stage3_ptb)
declare -a backround_data_outfiles=($stage1_bk $stage2_bk $stage3_bk)

#Generate the shuffled data

shuffle_bed_n $_feature_bed_file $_genomic_length_file $number_of_shuffles > $shuffled_data_file

#Loop through the arrays to perform the analysis

array_index=0

while [[ "$array_index" -lt "${#bedpe_input_files[@]}" ]]
do
	#Get the file paths corresponding to each stage
	bedpe_input=${bedpe_input_files[$array_index]}
	ptb_outfile=${pairtobed_outfiles[$array_index]}
	bk_outfile=${backround_data_outfiles[$array_index]}
	
	#Perform pairtobed with the actual feature locations
	
	pairtobed -a $bedpe_input -b $_feature_bed_file -type $_type -f $_f >$ptb_outfile

	#Perform pairtobed with the shuffled data
	
	pairtobed -a $bedpe_input -b $shuffled_data_file -type $_type -f $_f >$bk_outfile
	

	array_index=`expr $array_index + 1`
done


#Perform Statistics in R
pairtobed_commandline_pipeline.r "$stage1_ptb" "$stage1_bk" "$stage2_ptb" "$stage2_bk" "$stage3_ptb" "$stage3_bk" \
				"$_experiment_name" "$backround" \
				"$_stage1_name" "$_stage2_name" "$_stage3_name" \
				"$_boxplot_title" "$_boxplot_outfile"


}

analyze_bedpe_by_stage "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9" "${10}" "${11}" "${12}" "${13}"



