#!/usr/bin/env python
__author__ = 'justingibbons'

import csv


def create_intervals(bin_size,total_size):
    """Takes 2 integers bin_size and total_size and returns a list of tuples representing a range of
        at most bin_size and all of the ranges summed together equals total_size. Indexing is 0 based and the last
            number in a range should not be considered a member of that range (ie (0,11) is actually ends at 10."""

    out_list=[]

    end_index=bin_size
    start_index=0

    #Start creating the intervals
    if bin_size>total_size:
        raise ValueError("bin_size is greater then total_size")

    if bin_size <= 0:
        raise ValueError("bin_size must be greater then zero")
    while end_index <= total_size:
        bin=(start_index,end_index)
        out_list.append(bin)
        start_index=end_index
        end_index+=bin_size
        current_end_index=bin[1]

    #See if all of the bins were created
    if current_end_index==total_size:
        return out_list
    #If the final index isn't equal to total_size bin_size must be adjusted
    elif current_end_index+bin_size>total_size:
        #Adjust bin size to include the final bin
        amount_excess=end_index-total_size
        adjusted_bin_size=bin_size-amount_excess
        final_end_index=start_index+adjusted_bin_size
        bin=(start_index,final_end_index)
        out_list.append(bin)
        return out_list
    else:
        print "Something weird happend: current_end_index_bin_size>total_size. That shouldn't happen!"


def create_intervals_from_genomic_file(infile,outfile,bin_size=10000):
    """Takes in a file containing the lengths of the different chromosomes with chr and length being tab-sep and
    returns a tab-delimited file containing the start and end indexes of size bin_size that fit on that chromosome"""
    with open(outfile,"w") as binned_genome:
        csv_writer=csv.writer(binned_genome,delimiter="\t")

        with open(infile,"r") as genome_length:
            csv_reader=csv.reader(genome_length,delimiter="\t")
            for zome in csv_reader:
                zome_name=zome[0]
                zome_len=zome[1]
                bins=create_intervals(bin_size,int(zome_len))
                for bin in bins:
                    row=[zome_name]
                    row.extend(bin)
                    csv_writer.writerow(row)

if __name__ =="__main__":
    import sys

    if len(sys.argv)==1: #If no command line arguments given run the tests

        import unittest

        class TestBinData(unittest.TestCase):
            def test_create_intervals_bin_size_too_large(self):
                with self.assertRaises(ValueError):
                    create_intervals(10000,10)

            def test_create_intervals_bin_size_not_positive_integer(self):
                with self.assertRaises(ValueError):
                    create_intervals(0,10)
            def test_create_intervals_even_bin_even_total_size(self):
                self.assertEqual(create_intervals(2,10),[(0,2),(2,4),(4,6),(6,8),(8,10)])
            def test_create_intervals_odd_bin_even_total_size(self):
                self.assertEqual(create_intervals(3,10),[(0,3),(3,6),(6,9),(9,10)])
            def test_create_intervels_even_bin_odd_total_size(self):
                self.assertEqual(create_intervals(2,11),[(0,2),(2,4),(4,6),(6,8),(8,10),(10,11)])
            def test_create_intervals_odd_bin_and_total_size(self):
                self.assertEqual(create_intervals(3,11),[(0,3),(3,6),(6,9),(9,11)])
            def test_create_intervals_bin_size_of_one(self):
                self.assertEqual(create_intervals(1,5),[(0,1),(1,2),(2,3),(3,4),(4,5)])

            def test_create_interval_from_genomic_file(self):
                infile="in_files/small_chromosome_lengths.txt"
                outfile="out_files/result_create_intervals_from_genomic_file.txt"
                ref_file="reference_files/ref_create_intervals_from_genomic_file.txt"
                create_intervals_from_genomic_file(infile,outfile,2)
                with open(outfile,'r') as results_file:
                    results=results_file.readlines()
                with open(ref_file) as ref:
                    ref_data=ref.readlines()
                self.assertEqual(results,ref_data)

        unittest.main()

    else: #Otherwise run create_interval_from_genomic_file using commandline arguments
        infile=sys.argv[1]
        outfile=sys.argv[2]
        try:
            bin_size=int(sys.argv[3]) #bin_size is optional
            create_intervals_from_genomic_file(infile,outfile,bin_size)
        except IndexError:
            create_intervals_from_genomic_file(infile,outfile)

