library(gplots)
library(RColorBrewer)
exp1="in_files/counts_per_bin.bed"
exp2="in_files/counts_per_bin2.bed"
exp3="in_files/counts_per_bin3.bed"

input="in_files/input_counts_per_bin.bed"


# exp1="/Users/justingibbons/epigenetic_data/GSE23787_RAW/H2A.Z/sorted_counted/sorted_counted_GSM588510_P.falciparum_3D7_ChIPseq_H2A.Z_10hpi.bed"
# exp2="/Users/justingibbons/epigenetic_data/GSE23787_RAW/H2A.Z/sorted_counted/sorted_counted_GSM588511_P.falciparum_3D7_ChIPseq_H2A.Z_20hpi.bed"
# 
# input1="/Users/justingibbons/epigenetic_data/GSE23787_RAW/input/sorted_counted/sorted_counted_GSM588523_P.falciparum_3D7_ChIPseq_input_10hpi.bed"
# input2="/Users/justingibbons/epigenetic_data/GSE23787_RAW/input/sorted_counted/sorted_counted_GSM588524_P.falciparum_3D7_ChIPseq_input_20hpi.bed"

read_coveragebed_count_results<-function(input_file,col1="chr",col2="bin_start",col3="bin_end",col4="bin_count",stage){
  
  #Creates a dataframe from the results of using coveragebed with the -counts option 
  v_colnames=c(col1,col2,col3,col4)
  df=read.delim(input_file,header=FALSE,col.names=(v_colnames))
  df$stage=stage
  
  return(df)
}

calc_RPKM<-function(df){
  #Takes in a data frame with row chr bin_start bin_end bin_count and returns a vector containing the
  # RPKM for each bin
  #RPKM is : R=10^9C/NL were C is the number of reads in the bin (bin_count), N is the total number of reads (the sum of bin_count)
  # and L is the size of the bin (bin_end-bin_start). Note 1 will first be add to all of the reads to ensure division by zero does not occur 
  # when calculating fold change latter (ie RPKM(experiment)/RPKM(input))
  
  v_rpkm=10^9*(df$bin_count+1)/sum(df$bin_count)*(df$bin_end-df$bin_start)
  return(v_rpkm)
  
}

add_RPKM_to_dataframe<-function(df){
  #Adds RPKM to a dataframe containing bin seq counts.
  #Input dataframe has columns chr bin_start bin_end bin_count
  #Output dataframe has columns chr bin_start bin_end bin_count rpkm
  #If this function works then so does calc_RPKM
  v_rpkm<-calc_RPKM(df)
  df$rpkm=v_rpkm
  return (df)
  
}

calc_RPKM_fold_change<-function(df_experiment,df_input){
  #Returns a vector of log2(RPKM(experiment)/RPKM(input))
  #This assumes the experiment and input are sorted the same way.
  
  return(log2(calc_RPKM(df_experiment)/calc_RPKM(df_input)))
}

add_rpkm_fold_change_to_experimental_dataframe<-function(df_experiment,df_input){
  #Creates a copy of df_experiment with the column rpkm_fold_change add to it.
  #The fold change is calculated like RPKM(experiment)/RPKM(input)
  
  df_experiment$rpkm_fold_change=calc_RPKM_fold_change(df_experiment,df_input)
  return(df_experiment)
}

write_rpkm_fold_change_out<-function(df,outfile,keep_rows_bool=FALSE,keep_cols_bool=FALSE,quote_bool=FALSE){
  #Takes the df and writes out the columns: chr bin_start bin_end rpkm_fold_change
  df_new=data.frame(chr=df$chr,bin_start=df$bin_start,bin_end=df$bin_end,
                    rpkm_fold_change=df$rpkm_fold_change)
  write.table(df_new,file=outfile,sep="\t",row.names=keep_rows_bool,col.names=keep_cols_bool,quote=quote_bool)
}

calc_bin_diff_between_stages_zscore<-function(list_with_2_dataframes){
  #Takes a list containing 2 dataframes (makes it easier to automate)
  #Assumes the stages are both sorted the same way
  #Calculates the difference in RPKM(experiment)/RPKM(input) between 2 stages (or experimental conditions), and their zscores.
  #Returns a new dataframe in the form
  # chr bin_start bin_end first_stage second_stage first_stage_rpkm_fold_change second_stage_rpkm_fold_change abs_rpkm_diff_zscore
  #note first_stage and second_stage may not be in chronological order
  #Note this requires the RPKM to already be calculated and added to the dataframe. This creates more flexibility to if you want
  #To use different input data to normalize the different conditions (though I'm not sure why you would do that).
  #Returns both dataframes with their zscores appened.
  df_stage1=list_with_2_dataframes[[1]]
  df_stage2=list_with_2_dataframes[[2]]
  #Get the dataframes out of the vector
  #Get the zscore
  rpkm_fold_change_diff=df_stage1$rpkm_fold_change-df_stage2$rpkm_fold_change
  rpkm_fold_change_diff_mean=mean(rpkm_fold_change_diff)
  rpkm_fold_change_diff_std=sd(rpkm_fold_change_diff)
  rpkm_fold_change_diff_z=abs((rpkm_fold_change_diff-rpkm_fold_change_diff_mean))/rpkm_fold_change_diff_std
  
  #Make the new dataframe
  df_return=data.frame(chr=df_stage1$chr,bin_start=df_stage1$bin_start,bin_end=df_stage1$bin_end,
                      first_stage=df_stage1$stage,second_stage=df_stage2$stage,
                      first_stage_rpkm_fold_change=df_stage1$rpkm_fold_change,
                      second_stage_rpkm_fold_change=df_stage2$rpkm_fold_change,
                      abs_rpkm_diff_zscore=rpkm_fold_change_diff_z)
  
return(df_return)
  
}

pairwise_calc_bin_diff_between_stages_zscore<-function(df_stage_list){
  #Applies calc_bin_diff_between_stages_zscore to all pairwise combinations of dataframes in
  #df_stage_list and returns the results as a single dataframe.
  
  return(do.call("rbind",combn(df_stage_list,m=2,FUN=calc_bin_diff_between_stages_zscore,simplify=FALSE)))
}

filter_bins_by_diff_between_stages_zscore<-function(df,min_zscore){
  #Takes the output of pairwise_calc_bin_diff_between_stages_zscore and filters it so that
  #only results with abs_rpkm_diff_zscore greater then or equal to min_zscore remain
  new_df=subset(df,subset=df$abs_rpkm_diff_zscore >=min_zscore)
  return(new_df)
}


#####Probably won't use functions below this point.###############

combine_dataframes_by_stage<-function(list_of_dataframes,list_of_stage_names){
  ####This is probably deprecated since I included the stage column in read_coveragebed_count_results
  #Creates a single dataframe out of a list of dataframes with a stage column.
  #The stage is assigened based on the position of the dataframe in the list_of_dataframes
  for(i in 1:length(list_of_stage_names)){
    list_of_dataframes[[i]]$stage<-list_of_stage_names[i]
      }
  return(do.call("rbind",list_of_dataframes))
}

make_heat_map_with_stage_columns<-function(dataframe,stages,main=NULL){
  #Uses rpkm_fold_change to make a heatmap where the different stages are represented by the columns.
  #stages should be provided as a list in the order you want them to appear on the heatmap
  
  #May want to explicitly reorder the dataframe by bin_start
  l_matrix_columns=list()
  for(s in stages){
    temp_df=subset(dataframe,subset=dataframe$stage==s)
    l_matrix_columns[[length(l_matrix_columns)+1]]=temp_df$rpkm_fold_change
  }
  m_fold_change=do.call("cbind",l_matrix_columns)
  colnames(m_fold_change)=stages
  rownames(m_fold_change)=temp_df$bin_start
  heatmap.2(m_fold_change,Rowv=FALSE,dendrogram="none", col=brewer.pal(9,"YlOrRd"),trace="none",margins=c(4,5),
            cexCol=0.5,cexRow=0.75,key.title="Fold Change",main=main)
}

make_heat_maps_by_zome_and_stage<-function(dataframe,stages){
  #Writes heatmaps to file were each heatmap cooresponds to a different chromosome and each column
  #corresponds to a different stage in stages
  zomes=unique(dataframe$chr)
  for(zome in zomes){
    outfile=paste(zome,"png",sep=".")
    png(outfile) #open the outfile to write the graph to
    temp_df=subset(dataframe,subset=dataframe$chr==zome)
    make_heat_map_with_stage_columns(temp_df,stages,main=zome)
    dev.off() #close the outfile
    
  }
}

df_exp1<-read_coveragebed_count_results(exp1,stage="stage1")
df_exp2<-read_coveragebed_count_results(exp2,stage="stage2")
df_exp3<-read_coveragebed_count_results(exp3,stage="stage3")
df_input<-read_coveragebed_count_results(input,stage="placeholder")

df_exp1_rpkm<-add_rpkm_fold_change_to_experimental_dataframe(df_exp1,df_input)
df_exp2_rpkm<-add_rpkm_fold_change_to_experimental_dataframe(df_exp2,df_input)
df_exp3_rpkm<-add_rpkm_fold_change_to_experimental_dataframe(df_exp3,df_input)

write_rpkm_fold_change_out(df_exp1_rpkm,"out_files/result_write_rpkm_fold_change_out.bed")

#View(df_exp1_rpkm)

#df_zscore_dataframes<-calc_bin_diff_between_stages_zscore(list(df_exp2_rpkm,df_exp3_rpkm))
#View(df_zscore_dataframes)

# df_exp1<-read_coveragebed_count_results(exp1,stage="stage1")
# df_exp2<-read_coveragebed_count_results(exp2,stage="stage2")
# df_input1<-read_coveragebed_count_results(input1)
# df_input2<-read_coveragebed_count_results(input2)

#df_exp1_rpkm<-add_rpkm_fold_change_to_experimental_dataframe(df_exp1,df_input1)
#df_exp2_rpkm<-add_rpkm_fold_change_to_experimental_dataframe(df_exp2,df_input2)

# df_combined<-combine_dataframes_by_stage(list(df_exp1_rpkm,df_exp2_rpkm),list("Early_Ring","Late_Ring"))
#make_heat_map_with_stage_columns(df_combined,list("Early_Ring","Late_Ring"),main="Test")
#make_heat_maps_by_zome_and_stage(df_combined,list("Early_Ring","Late_Ring"))

df_combo_zscore<-pairwise_calc_bin_diff_between_stages_zscore(list(df_exp1_rpkm,df_exp2_rpkm,df_exp3_rpkm))
View(df_combo_zscore)
df_combo_zscore_filtered<-filter_bins_by_diff_between_stages_zscore(df_combo_zscore,1.3)
View(df_combo_zscore_filtered)
