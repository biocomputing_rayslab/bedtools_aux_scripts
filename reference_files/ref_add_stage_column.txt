chrom1	start1	end1	chrom2	start2	end2	score	chrom3	start3	end3	feature_strand	feature_category	feature_name	stage
chr1	630000	640001	chr4	560000	570001	18.876037	chr1	630586	638444	+	cat1	feature1	sample
chr1	630000	640001	chr4	560000	570001	18.876037	chr4	563024	570638	-	cat2	feature2	sample
chr2	10000	20001	chr7	180000	190001	12.80013	chr2	11551	19308	+	cat3	feature3	sample
chr2	10000	20001	chr7	180000	190001	12.80013	chr7	183409	191309	-	cat4	feature4	sample
