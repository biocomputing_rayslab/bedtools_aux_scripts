#!/usr/bin/env Rscript

#Put the files in the order you want the boxplots to be created.
#For each boxplot put in the experimental data followed by the backround data
#for example: stage1_data stage1_backround stage2_data stage2_backround ...
#After the data enter in the name of the experimental data and the backround
#to be shown underneath the boxplots,then the 3 stage names,then
#the title of the boxplot and then the outfile.

#May want to improve this by using some sort of commandline parsing module

#########SubFunctions. Origninally Taken from pairtobed_results_pipeline.R#####
read_pairtobed_results<-function(input_file,col1="chrom1",col2="start1",col3="end1",
                                 col4="chorm2", col5="start2", col6="end2",col7="score",col8="chrom3",col9="start3", col10="end3",
                                 col11="feature_strand",col12="feature_category",col13="feature_name"){
  
  #Creates a dataframe from the pairtobed results using the specified column names. Please note no more
  #then 12 columns can be specified. 
  v_colnames=c(col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13)
  df=read.delim(input_file,header=FALSE)
  #Add names for the required columns
  colnames(df)[c(1:10)]<-v_colnames[c(1:10)]
  
  #Try adding the remaining 3 optional colnames. Improve by adding messages
  try(colnames(df)[11]<-v_colnames[11],silent=TRUE)
  try(colnames(df)[12]<-v_colnames[12],silent=TRUE)
  try(colnames(df)[13]<-v_colnames[13],silent=TRUE)
  
  return(df)
}

boxplots_by_stage<-function(experimental_data,backround_data
                            ,names,ylab,main,stage_labels,median_label_dist=2,y_axis_stage_label=NULL,outfile){
  #Written to boxplot experimental and backround data for ring, schizont and trophozoite stages of the
  #malaria life cycle but should work well whenever there are 3 groups of data with comparision 
  #measurements from each. Names refers to the labels under each boxplot.
  #median_label_dist can be used to adjust median labels distance form the median for each boxplot
  #y_axis_stage_label is where the stage_labels should appear on the yaxis.
  
  #Adapted from Stephens Code
  
  #Get the data
  stage1_data=experimental_data[[1]]$score
  stage2_data=experimental_data[[2]]$score
  stage3_data=experimental_data[[3]]$score
  
  stage1_br=backround_data[[1]]$score
  stage2_br=backround_data[[2]]$score
  stage3_br=backround_data[[3]]$score
  
  png(outfile)
  
  #Make the Boxplot
  boxplot_data=boxplot(stage1_data,stage1_br,stage2_data,stage2_br,stage3_data,stage3_br,
                       names=names,ylab=ylab,main=main,outline=FALSE)
  
  #Label the Median
  
  text(x=1,y=(boxplot_data$stats[3,1] + median_label_dist), labels=boxplot_data$stats[3,1], cex=0.7)
  text(x=2,y=(boxplot_data$stats[3,2] + median_label_dist), labels=boxplot_data$stats[3,2], cex=0.7)
  text(x=3,y=(boxplot_data$stats[3,3] + median_label_dist), labels=boxplot_data$stats[3,3], cex=0.7)
  text(x=4,y=(boxplot_data$stats[3,4] + median_label_dist), labels=boxplot_data$stats[3,4], cex=0.7)
  text(x=5,y=(boxplot_data$stats[3,5] + median_label_dist), labels=boxplot_data$stats[3,5], cex=0.7)
  text(x=6,y=(boxplot_data$stats[3,6] + median_label_dist), labels=boxplot_data$stats[3,6], cex=0.7)
  
  #Label the stages
  if (is.null(y_axis_stage_label)){
    #if no vaule was set for y_axis_stage_label just make sure it ends up on the plot
    y_axis_stage_label=max(boxplot_data$stats[5,])*0.9 #Make it a little smaller then the max so it doesn't overlap the bar
  }
  text(x=1.5,y=y_axis_stage_label,label=stage_labels[1])
  text(x=3.5,y=y_axis_stage_label,label=stage_labels[2])
  text(x=5.5,y=y_axis_stage_label,label=stage_labels[3])
  
  #Add the p-values calculated by the Mann-Whitney U test for each stage.
  
  stage1_stats=wilcox.test(stage1_data,stage1_br)
  stage2_stats=wilcox.test(stage2_data,stage2_br)
  stage3_stats=wilcox.test(stage3_data,stage3_br)
  
  y_axis_stage_label=y_axis_stage_label*0.8 #Add the label below the stage name
  
  text(x=1.5,y_axis_stage_label,paste("p",format.pval(stage1_stats$p.value),sep=" "))
  text(x=3.5,y_axis_stage_label,paste("p",format.pval(stage2_stats$p.value),sep=" "))
  text(x=5.5,y_axis_stage_label,paste("p",format.pval(stage3_stats$p.value),sep=" "))
  
  #Seperate Stages with lines
  
  abline(v=2.5)
  abline(v=4.5)
  
  dev.off() #close the outfile
}


read_in_data_and_create_boxplots<-function(experimental_data,backround_data,
                                           names,ylab="Contact Count",main,stage_labels,median_label_dist=2,y_axis_stage_label=NULL,outfile){
  
  #Takes 2 vectors one containing a file path to the experimental data and the other containing a file path to the
  #corresponding backround data. Also takes in names for the boxplots, ylab, main, stage_labels corresponding to the
  #index position of the first 2 vectors and ways to adjust placement of text in the boxplot
  
  list_of_experimental_dataframes=list()
  list_of_backround_dataframes=list()
  number_of_experiments=length(experimental_data)
  #Get the data
  for (i in 1:number_of_experiments){
    list_of_experimental_dataframes[[i]]=read_pairtobed_results(experimental_data[[i]])
    list_of_backround_dataframes[[i]]=read_pairtobed_results(backround_data[[i]])
  }
  boxplots_by_stage(list_of_experimental_dataframes,list_of_backround_dataframes,
                    names=names,ylab=ylab,main=main,stage_labels=stage_labels,outfile=outfile)
  
  
}
#######End Subfunctions############


args<-commandArgs(TRUE)

#Get the data
stage1_data=args[1]
stage1_bk=args[2]
stage2_data=args[3]
stage2_bk=args[4]
stage3_data=args[5]
stage3_bk=args[6]

experimental_name=args[7]
backround_name=args[8]

stage1_name=args[9]
stage2_name=args[10]
stage3_name=args[11]

main=args[12] #The title

outfile=args[13]

print(outfile)

experimental_data=list(stage1_data,stage2_data,stage3_data)
backround_data=list(stage1_bk,stage2_bk,stage3_bk)
stage_labels=list(stage1_name,stage2_name,stage3_name)
#names are the labels under the boxplots
names=list(experimental_name,backround_name,experimental_name,backround_name,experimental_name,backround_name)
read_in_data_and_create_boxplots(experimental_data,
                                 backround_data,
                                           names,
                                 ylab="Contact Count",
                                 main,
                                 stage_labels,
                                 median_label_dist=2,
                                 y_axis_stage_label=NULL,
                                 outfile)